GameState = require"gamestate"

local Menu = require"gamestates.menu"

function love.load()
    font = love.graphics.newFont("Comfortaa-Regular.ttf", 20)
    fontSmall = love.graphics.newFont("Comfortaa-Regular.ttf", 13)
    GameState:push(Menu)
end

function love.update(dt)
    if GameState:isEmpty() then
        love.event.quit()
    end
    GameState:update(dt)
end

for _, callback in ipairs{"draw", "keypressed", "keyreleased", "mousepressed", "mousereleased", "textinput"} do
    love[callback] = function(...)
        GameState[callback](GameState, ...)
    end
end
