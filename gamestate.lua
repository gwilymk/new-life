local GameState = {
    stack = {}
}

function GameState:push(state)
    table.insert(self.stack, state)
    if state.enter then
        state:enter(self.stack[#self.stack-1])
    end
end

function GameState:pop()
    if self.stack[#self.stack].leave then
        self.stack[#self.stack]:leave()
    end
    self.stack[#self.stack] = nil
end

function GameState:replace(state)
    local s = self.stack[#self.stack]
    if s.leave then s:leave() end
    self.stack[#self.stack] = state
    if state.enter then state:enter(s) end
end

function GameState:isEmpty()
    return #self.stack == 0
end

local callbacks = {"draw", "update", "mousepressed", "mousereleased", "keypressed", "textinput", "keyreleased"}

for _, callback in ipairs(callbacks) do
    GameState[callback] = function(self, ...)
        local state = self.stack[#self.stack]

        if state and state[callback] then
            state[callback](state, ...)
        end
    end
end

return GameState
