local GameEnd = {}

function GameEnd:enter(state)
    if (state.playernumber - 1.5) * state.board.score > 0 then
        self.winner = true
    else
        self.winner = false
    end
end

function GameEnd:draw()
    love.graphics.setFont(font)
    love.graphics.setColor(255,255,255)

    if self.winner then
        love.graphics.print("You Win!!!", 100, 100)
    else
        love.graphics.print("You loose", 100, 100)
    end
end

function GameEnd:keypressed()
    GameState:pop()
end

return GameEnd
