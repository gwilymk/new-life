local socket = require"socket"
local net = require"net.utils"

local Loading = {}

local Playing = require"gamestates.playing"

function Loading:enter(menu)
    self.socket = socket.tcp()
    assert(self.socket:connect(net.ADDRESS, net.PORT))
    self.socket:settimeout(0.01)
    self.name = menu.name
end

function Loading:update()
    local data, err = net.receive(self.socket, 0.01)
    if data then
        if data.version and data.version ~= net.VERSION then
            self.socket:close()
            GameState:pop()
            return
        end
        if data.command == "setup" then
            self.playernumber = data.playernumber
            net.send(self.socket, {command = "setup", name = self.name})
        elseif data.command == "starting" then
            self.name1 = data.player1
            self.name2 = data.player2

            GameState:replace(Playing)
        end
    end
end

function Loading:draw()
    love.graphics.setFont(font)
    love.graphics.print("Looking for an opponent...", 100, 100)
end

return Loading
