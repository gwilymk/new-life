local Loading = require"gamestates.loading"

local Menu = {
    name = "Player",
}

function Menu:enter()
    love.keyboard.setKeyRepeat(true)
end

function Menu:leave()
    love.keyboard.setKeyRepeat(false)
end

function Menu:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(font)
    love.graphics.print(string.format("Name: %s*", self.name), 50, 50)
end

function Menu:textinput(character)
    if #self.name < 16 then
        self.name = self.name .. character
    end
end

function Menu:keypressed(key)
    if key == "backspace" then
        self.name = string.sub(self.name, 1, -2)
    end

    if key == "return" then
        if #self.name > 0 then
            GameState:push(Loading)
        end
    end
end

return Menu
