local Board = require"board"
local GameEnd = require"gamestates.end"
local Playing = {}
local net = require"net.utils"

local CELL_SIZE = math.min(32, love.graphics.getWidth() / (Board.HORIZONTAL + 2), (love.graphics.getHeight() - 50) / (Board.VERTICAL+1))
local BOARD_X = (love.graphics.getWidth() / 2) - CELL_SIZE * Board.HORIZONTAL / 2
local BOARD_Y = love.graphics.getHeight() - CELL_SIZE * (Board.VERTICAL + 1)

local PLAYER_COLOURS = {
    {68, 43, 0}, -- player territory
    {225, 181, 38}, -- player cell
    {225, 192, 70}, -- player new cell
    {0, 47, 1}, -- opponent territory
    {0, 192, 14}, -- opponent cell
}


local COLORS = {
    {0, 0, 0},
    {36, 24, 0},
    {7, 4, 46},
    {225, 165, 0},
    {77, 2, 197},
    {0xff, 0x91, 0x27},
}


function drawWithCentre(text, x, y)
    local width = love.graphics.getFont():getWidth(text)
    local height = love.graphics.getFont():getHeight()

    love.graphics.print(text, x - width / 2, y - height / 2)
end

function Playing:enter(loading)
    self.socket = loading.socket
    self.name = loading.name
    self.playernumber = loading.playernumber
    self.name1 = loading.name1
    self.name2 = loading.name2
    self.boardCanvas = love.graphics.newCanvas(Board.HORIZONTAL * CELL_SIZE, Board.VERTICAL * CELL_SIZE)
    self.board = Board()

    self.waitingForOtherPlayer = false

    if self.playernumber == 1 then
        self.territoryNumber = Board.PLAYER1_TERRITORY
        self.cellNumber = Board.PLAYER1_CELL
        self.newCellNumber = Board.PLAYER1_NEW_CELL
        for i = 1,3 do
            COLORS[2*i] = PLAYER_COLOURS[i]
            COLORS[2*i+1] = PLAYER_COLOURS[i+3]
        end
    elseif self.playernumber == 2 then
        self.territoryNumber = Board.PLAYER2_TERRITORY
        self.cellNumber = Board.PLAYER2_CELL
        self.newCellNumber = Board.PLAYER2_NEW_CELL
        for i = 1,3 do
            COLORS[2*i] = PLAYER_COLOURS[i+3]
            COLORS[2*i+1] = PLAYER_COLOURS[i]
        end
    else
        assert(false, "Player number should be 1 or 2")
    end

    self.numPlayer = {0, 0}
end

function Playing:mousereleased(x, y, button)
    local gridX = math.floor((x - BOARD_X) / CELL_SIZE) + 1
    local gridY = math.floor((y - BOARD_Y) / CELL_SIZE) + 1

    if button == 'l' and not self.waitingForOtherPlayer then
        if gridX > 0 and gridX <= Board.HORIZONTAL and gridY > 0 and gridY <= Board.VERTICAL then
            self.numPlayer[self.playernumber] = self.numPlayer[self.playernumber] + self.board:toggle(self.playernumber, gridX, gridY)
        end
    elseif button == 'r' then
        if not self.waitingForOtherPlayer then
            self.waitingForOtherPlayer = true
            net.send(self.socket, {command = "turn-done", data = self.board:newCellList()})
        end
    end
end

function Playing:update()
    local data, err = net.receive(self.socket, 0.01)
    if err == "closed" then
        GameState:pop()
        return
    end

    if data then
        if data.command == "next-turn" then
            local done
            self.board:applyNewCells(3 - self.playernumber, data.data[3 - self.playernumber])
            done, self.numPlayer = self.board:simulate()
            self.waitingForOtherPlayer = false

            if done then
                self.socket:close()
                GameState:replace(GameEnd)
            end
        end
    end
end

function Playing:draw()
    local oldCanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.boardCanvas)
    love.graphics.clear()

    for i = 1,Board.HORIZONTAL do
        for j = 1,Board.VERTICAL do
            love.graphics.setColor(COLORS[self.board[i][j]])
            love.graphics.rectangle('fill', (i - 1)*CELL_SIZE, (j - 1)*CELL_SIZE, CELL_SIZE, CELL_SIZE)
        end
    end

    love.graphics.setColor(170,170,170)

    for x = 1,Board.HORIZONTAL + 1 do
        love.graphics.line((x - 1) * CELL_SIZE, 0, (x - 1) * CELL_SIZE, Board.VERTICAL * CELL_SIZE)
    end

    for y = 1,Board.VERTICAL + 1 do
        love.graphics.line(0, (y - 1) * CELL_SIZE, Board.HORIZONTAL * CELL_SIZE, (y - 1) * CELL_SIZE)
    end

    love.graphics.setCanvas(oldCanvas)
    love.graphics.draw(self.boardCanvas, BOARD_X, BOARD_Y)

    if self.waitingForOtherPlayer then
        love.graphics.setFont(font)
        drawWithCentre("WAITING FOR OTHER PLAYER", love.graphics.getWidth() / 2, CELL_SIZE)
    end

    -- Draw player information
    -- player 1
    love.graphics.setColor(255,255,255)
    love.graphics.setFont(fontSmall)
    love.graphics.print(self.name1, CELL_SIZE * 4.5, CELL_SIZE * 0.5)
    love.graphics.setColor(COLORS[4])
    for i = 1,self.board.playerNewCells[1] do
        love.graphics.rectangle('fill', 0.5*CELL_SIZE*i+CELL_SIZE*4, CELL_SIZE * 1.25, 0.5 * CELL_SIZE - 2, 0.5 * CELL_SIZE - 2)
    end

    love.graphics.rectangle('fill', CELL_SIZE, CELL_SIZE, CELL_SIZE, CELL_SIZE)
    love.graphics.setColor(255,255,255)
    love.graphics.setFont(font)
    drawWithCentre(tostring(self.numPlayer[1]), CELL_SIZE * 1.5, CELL_SIZE * 1.5)

    -- player 2
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(fontSmall)
    love.graphics.print(self.name2, love.graphics.getWidth() - CELL_SIZE * 4 - Board.MAX_CELLS_PER_TURN * CELL_SIZE * 0.5, CELL_SIZE * 0.5)
    love.graphics.setColor(COLORS[5])
    for i = 1,self.board.playerNewCells[2] do
        love.graphics.rectangle('fill', love.graphics.getWidth() - 0.5 * CELL_SIZE * i - CELL_SIZE * 4, CELL_SIZE * 1.25, 0.5*CELL_SIZE - 2, 0.5 * CELL_SIZE - 2)
    end

    love.graphics.rectangle('fill', love.graphics.getWidth() - 2 * CELL_SIZE, CELL_SIZE, CELL_SIZE, CELL_SIZE)
    love.graphics.setColor(255,255,255)
    love.graphics.setFont(font)
    drawWithCentre(tostring(self.numPlayer[2]), love.graphics.getWidth() - CELL_SIZE * 1.5, CELL_SIZE * 1.5)

    -- score
    love.graphics.setColor(COLORS[self.board.score > 0 and 4 or 5])

    love.graphics.rectangle('fill', love.graphics.getWidth() / 2, CELL_SIZE * 2, -math.tanh(self.board.score / 200) * CELL_SIZE * 0.5 * Board.HORIZONTAL, CELL_SIZE / 2)
    love.graphics.setColor(255,255,255)
    love.graphics.setFont(fontSmall)
    drawWithCentre(string.format("Score: %i", math.abs(self.board.score)), love.graphics.getWidth() / 2, CELL_SIZE * 3)
end

return Playing
