local msgpack = require"libs.MessagePack"

local function send(socket, data)
    local str = msgpack.pack(data)
    return socket:send(string.format("%i\n%s", #str, str))
end

local function receive(socket, timeout)
    timeout = timeout or 0
    local length, err = socket:receive("*l")
    if err then return nil, err end

    if length then
        socket:settimeout(0)
        local data, err = socket:receive(tonumber(length))
        if err ~= nil then return nil, err end
        local ok, message = pcall(msgpack.unpack, data)

        socket:settimeout(timeout)

        if ok then
            return message
        else
            print "Extracting messagepack unsuccessful"
        end
    end
end

return {
    send = send,
    receive = receive,
    PORT = 26354,
    VERSION = 2,
    ADDRESS = "ld31.gwilym.ninja"
}
