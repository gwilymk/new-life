local Board = {
    HORIZONTAL = 20,
    VERTICAL = 14,

    CLEAR = 1,
    PLAYER1_TERRITORY = 2,
    PLAYER2_TERRITORY = 3,
    PLAYER1_CELL = 4,
    PLAYER2_CELL = 5,
    PLAYER1_NEW_CELL = 6,
    PLAYER2_NEW_CELL = 7,

    MAX_CELLS_PER_TURN = 5
}

if not table.unpack then table.unpack = unpack end

function Board:toggle(playernumber, x, y)
    local territoryNumber, newCellNumber
    if playernumber == 1 then
        territoryNumber = Board.PLAYER1_TERRITORY
        newCellNumber = Board.PLAYER1_NEW_CELL
    elseif playernumber == 2 then
        territoryNumber = Board.PLAYER2_TERRITORY
        newCellNumber = Board.PLAYER2_NEW_CELL
    end

    local newCells = self.playerNewCells[playernumber]

    if self[x][y] == territoryNumber and newCells >= 1 then
        self.playerNewCells[playernumber] = newCells - 1
        self[x][y] = newCellNumber
        return 1
    elseif self[x][y] == newCellNumber then
        self[x][y] = territoryNumber
        self.playerNewCells[playernumber] = newCells + 1
        return -1
    end

    return 0
end

function Board:newCellList()
    local list = {}

    for i = 1,Board.HORIZONTAL do
        for j = 1,Board.VERTICAL do
            if self[i][j] == Board.PLAYER1_NEW_CELL then
                table.insert(list, {i, j})
                self[i][j] = Board.PLAYER1_CELL
            elseif self[i][j] == Board.PLAYER2_NEW_CELL then
                table.insert(list, {i, j})
                self[i][j] = Board.PLAYER2_CELL
            end
        end
    end

    return list
end

function Board:applyNewCells(playernumber, list)
    local territoryNumber = (playernumber == 1) and Board.PLAYER1_TERRITORY or Board.PLAYER2_TERRITORY
    local cellNumber = (playernumber == 1) and Board.PLAYER1_CELL or Board.PLAYER2_CELL

    if type(list) ~= 'table' then return false end
    self.playerNewCells[playernumber] = self.playerNewCells[playernumber] - #list
    if self.playerNewCells[playernumber] < 0 then return false end

    for _, position in ipairs(list) do
        local x, y = table.unpack(position, 1, 2)
        if type(x) ~= 'number' or type(y) ~= 'number' then return false end
        if x <= 0 or x > Board.HORIZONTAL or y <= 0 or y > Board.VERTICAL then return false end

        if self[x][y] == territoryNumber then
            self[x][y] = cellNumber
        else
            return false
        end
    end

    return true
end

function Board:getCell(x, y)
    return self[(x-1) % Board.HORIZONTAL + 1][(y - 1) % Board.VERTICAL + 1]
end

function Board:simulate()
    local newBoard = {}
    local empty = true
    for x = 1,Board.HORIZONTAL do
        newBoard[x] = {}
        for y = 1,Board.VERTICAL do
            if self[x][y] == Board.CLEAR then
                empty = false
            end

            local total = 0
            local player = 0

            for i = -1,1 do
                for j = -1,1 do
                    local neighbour = self:getCell(x + i, y + j)
                    if neighbour == Board.PLAYER1_CELL then
                        total = total + 1
                        player = player + 1
                    elseif neighbour == Board.PLAYER2_CELL then
                        total = total + 1
                        player = player - 1
                    end
                end
            end

            if total == 3 then
                if self[x][y] == Board.CLEAR or self[x][y] == Board.PLAYER1_TERRITORY or self[x][y] == Board.PLAYER2_TERRITORY then
                    newBoard[x][y] = (player > 0) and Board.PLAYER1_CELL or Board.PLAYER2_CELL
                end
            elseif total ~= 4 then
                if self[x][y] == Board.PLAYER1_CELL then
                    newBoard[x][y] = Board.PLAYER1_TERRITORY
                elseif self[x][y] == Board.PLAYER2_CELL then
                    newBoard[x][y] = Board.PLAYER2_TERRITORY
                end
            end
        end
    end
    local numPlayer1 = 0
    local numPlayer2 = 0

    for i = 1,Board.HORIZONTAL do
        for j = 1,Board.VERTICAL do
            self[i][j] = newBoard[i][j] or self[i][j]
            if self[i][j] == Board.PLAYER1_CELL then
                numPlayer1 = numPlayer1 + 1
            elseif self[i][j] == Board.PLAYER2_CELL then
                numPlayer2 = numPlayer2 + 1
            end
        end
    end

    self.score = self.score + numPlayer1 - numPlayer2

    for i = 1,2 do
        self.playerNewCells[i] = math.min(self.playerNewCells[i] + 1, Board.MAX_CELLS_PER_TURN)
    end

    return empty, {numPlayer1, numPlayer2}
end

Board.__index = Board

return setmetatable(Board, {
    __call = function(self)
        local b = setmetatable({}, Board)
        for i = 1,Board.HORIZONTAL do
            b[i] = {}
            for j = 1,Board.VERTICAL do
                b[i][j] = Board.CLEAR
            end
        end

        -- fill in the initial terratory

        for i = 4,7 do
            for j = 6,9 do
                if (i == 4 or i == 7) and (j == 6 or j == 9) then
                    goto continue
                end

                b[i][j] = Board.PLAYER1_TERRITORY
                b[Board.HORIZONTAL - i + 1][j] = Board.PLAYER2_TERRITORY

                ::continue::
            end
        end

        b.playerNewCells = {Board.MAX_CELLS_PER_TURN, Board.MAX_CELLS_PER_TURN}
        b.score = 0

        return b
    end
})
