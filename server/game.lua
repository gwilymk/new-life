local net = require"net.utils"
local Board = require"board"

local Game = {}
function Game:receive(socket, data)
    if self.started then

    else
        if data.command == "setup" then
            if socket == self.s1 then
                self.name1 = tostring(data.name)
                if #self.name1 > 12 then return true end
            else
                self.name2 = tostring(data.name)
                if #self.name2 > 12 then return true end
            end

            if self.name1 and self.name2 then
                self:broadcast({
                    command = "starting",
                    player1 = self.name1,
                    player2 = self.name2
                })
            end
        elseif data.command == "turn-done" then
            if self.playerMoves[self.players[socket]] then return true end
            if not self.board:applyNewCells(self.players[socket], data.data) then return true end

            self.playerMoves[self.players[socket]] = data.data

            if self.playerMoves[1] and self.playerMoves[2] then
                self:broadcast{
                    command = "next-turn",
                    data = self.playerMoves
                }
                local ended = (self.board:simulate())

                self.playerMoves = {}
                if ended then return true end
            end
        end
    end
end

function Game:broadcast(msg)
    net.send(self.s1, msg)
    net.send(self.s2, msg)
end

Game.__index = Game

return setmetatable(Game, {
    __call = function(self, s1, s2)
        local g = setmetatable({}, self)
        g.s1, g.s2 = s1, s2
        net.send(s1, {command = "setup", playernumber = 1})
        net.send(s2, {command = "setup", playernumber = 2})
        g.started = false
        g.donePlayers = {false, false}
        g.players = {[s1] = 1, [s2] = 2}
        g.board = Board()
        g.playerMoves = {}
        return g
    end
})
