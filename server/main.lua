local function log(format, ...)
    print(string.format("%i - %s", os.time(), string.format(format, ...)))
end

local socket = require"socket"
local net = require"net.utils"
local Game = require"server.game"

local Games = setmetatable({}, {__mode = 'k'})

local server = socket.tcp()
assert(server:bind('*', net.PORT))
assert(server:listen(2))
server:settimeout(0.01)
local Clients = {server}

local currentClient = nil

local function removeGame(socket)
    local s1, s2 = Games[socket].s1, Games[socket].s2
    s1:close()
    s2:close()

    table.remove(Clients, Clients[s2])
    table.remove(Clients, Clients[s1])
    Clients[s1] = nil
    Clients[s2] = nil
end

while true do
    local ready = socket.select(Clients)

    if ready[server] then
        local client = server:accept()
        table.insert(Clients, client)
        Clients[client] = #Clients/2 + 1
        net.send(client, {version = net.VERSION})

        log("Client connected from %s", client:getpeername())

        if currentClient then
            local game = Game(currentClient, client)
            game:broadcast{command="readying"}
            Games[client] = game
            Games[currentClient] = game
            currentClient = nil
            log("Two clients connected, starting game")
        else
            currentClient = client
        end
    end

    for _, socket in ipairs(ready) do
        if socket == server then
            goto continue
        end

--[=[        if socket == currentClient then
            log("Client tried to send message early, disconnecting")
            Clients[Clients[socket]] = nil
            Clients[socket] = nil
            socket:close()
            currentClient = nil
            goto continue
        end]=]

        local data, err = net.receive(socket)
        if err == "closed" then
            log("Client disconnected")
            removeGame(socket)
            goto continue
        end

        if type(data) == "table" then
            if Games[socket]:receive(socket, data) then
                removeGame(socket)
            end
        end

        ::continue::
    end
end

server:close()
